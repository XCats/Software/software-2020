package frc.robot.processors;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.drive.TankDriveBase;
import frc.robot.utils.sensors.XInu;

public class Positioner extends SubsystemBase {

	private final XInu mInu;
	private final TankDriveBase mDriveBase;

	private NetworkTable mCoordinateGuiContainer = NetworkTableInstance.getDefault().getTable("CoordinateGui");
	private NetworkTable mCustomTable = mCoordinateGuiContainer.getSubTable("RobotPosition");



	private DifferentialDriveOdometry mOdometry;
	private double mX;
	private double mY;
	private double mAngle;
	private double mPreviousDistance;

	public Positioner(TankDriveBase drivetrain, XInu inu) {
		mDriveBase = drivetrain;
		mInu = inu;
		mCoordinateGuiContainer.getEntry(".type").setString("CoordinateGui");
	}

	@Override
	public void periodic() {
		mAngle = mInu.getRawPitch();
		mX = Units.metersToInches(mOdometry.getPoseMeters().getTranslation().getX());
		mY = Units.metersToInches(mOdometry.getPoseMeters().getTranslation().getY());
		mAngle = mOdometry.getPoseMeters().getRotation().getDegrees();
		mOdometry.update(new Rotation2d(Math.toRadians(-mAngle)), mDriveBase.getLeftEncoderDistance(), mDriveBase.getRightEncoderDistance());
		mCoordinateGuiContainer.getEntry("X").setDouble(mX);
		mCoordinateGuiContainer.getEntry("Y").setDouble(mY);
		mCoordinateGuiContainer.getEntry("Angle").setDouble(mAngle);
	}

	public void setPosition(double X, double Y, double Angle) {
		mDriveBase.zeroEncoders();
		mOdometry.resetPosition(new Pose2d(Units.inchesToMeters(X), Units.inchesToMeters(Y), new Rotation2d(Units.degreesToRadians(Angle))),  new Rotation2d(Units.degreesToRadians(Angle)));
	}

	public double getX() {
		return mX;
	}

	public double getY() {
		return mY;
	}

	public double getAngle() {
		return mAngle;
	}

}
