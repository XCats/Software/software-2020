package frc.robot.utils.triggers;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants;

public class StickTrigger extends Trigger {
    private double mDeadband = Constants.DEFAULT_JOYSTICK_DEADBAND.getValue();
    private final GenericHID mJoystick;
    private final int mAxis; 
    public StickTrigger(GenericHID stick, int axis) {
        this.mAxis = axis;
        this.mJoystick = stick;

    }

    public StickTrigger(GenericHID stick, int axis, double deadband) {
        this.mAxis = axis;
        this.mJoystick = stick;
        this.mDeadband = deadband;

    }

    @Override
    public boolean get() {
        return mJoystick.getRawAxis(mAxis) > mDeadband;
    }
}