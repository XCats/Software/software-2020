/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.utils.triggers;

import frc.robot.Constants;

/**
 * Add your docs here.
 */
public class DriveJoystickSmoother {
    private static double mDeadband = Constants.DEFAULT_JOYSTICK_DEADBAND.getValue();
    private static double mTopSpeed = Constants.DEFAULT_JOYSTICK_TOPSPEED.getValue();
    private static double minIncrement = Constants.DEFAULT_JOYSTICK_MIN_INCR.getValue();
    private static double prevLeft = 0.0;
    private static double prevRight = 0.0;
    private double mLeftJoySpeed = 0.0;
    private double mRightJoySpeed = 0.0;

    public DriveJoystickSmoother() {
    }

    public static double smooth(double speed) {
        mDeadband = Constants.DEFAULT_JOYSTICK_DEADBAND.getValue();
        mTopSpeed = Constants.DEFAULT_JOYSTICK_TOPSPEED.getValue();
        double newspeed = 0;
        if (Math.abs(speed) > mDeadband) {
            newspeed = speed * (Math.abs(speed) - mDeadband) / (mTopSpeed - mDeadband);
            //System.out.println(newspeed + "  smoother - deadband topspeed - " + mDeadband + "  " + mTopSpeed);
        }
        return newspeed;
    }

    public static void extrasmooth(double speed, boolean isLeft) {
        mDeadband = Constants.DEFAULT_JOYSTICK_DEADBAND.getValue();
        mTopSpeed = Constants.DEFAULT_JOYSTICK_TOPSPEED.getValue();
        minIncrement = Constants.DEFAULT_JOYSTICK_MIN_INCR.getValue();
        double newspeed = 0;
        double diff;
        if (Math.abs(speed) > mDeadband) {
            newspeed = speed * (Math.abs(speed) - mDeadband) / (mTopSpeed - mDeadband);
            //System.out.println(newspeed + "  smoother - deadband topspeed - " + mDeadband + "  " + mTopSpeed);
        }
        newspeed = (isLeft) ? prevLeft : prevRight;
        diff = speed - newspeed;
        if (Math.abs(diff) >= minIncrement) {
            newspeed = ((diff>0) ? newspeed + minIncrement : newspeed - minIncrement);
        } else if (speed != newspeed) { newspeed = speed; 
        } // else it is the same as before
        if (isLeft) { prevLeft = newspeed; }
        else { prevRight = newspeed; }
        //System.out.println("extrasmooth: left: " + prevLeft + "  right: " + prevRight);
    }

    public static double getLeft() {
        return prevLeft;
    }

    public static double getRight() {
        return prevRight;
    }

}
