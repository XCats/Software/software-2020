package frc.robot.utils;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;;

public class XTimerOverrideCommand<T extends CommandBase> extends CommandBase
{
    @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

    private double mOverrideTime;
    private T mCommand;
    private Timer mTimer = new Timer();

    /**
     * Creates a new ExampleCommand.
     *
     * @param subsystem The subsystem used by this command.
     */
    public XTimerOverrideCommand(double overrideTime, T command) {
        this.mOverrideTime = overrideTime;
        this.mCommand = command;


  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
      mTimer.reset();
      mTimer.start();
      mCommand.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      mCommand.execute();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      mCommand.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
      return mCommand.isFinished() || mOverrideTime <= mTimer.get();

  }
}
