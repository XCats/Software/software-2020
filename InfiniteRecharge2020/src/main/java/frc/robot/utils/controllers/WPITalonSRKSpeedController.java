package frc.robot.utils.controllers;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import frc.robot.Constants;

public class WPITalonSRKSpeedController implements XSpeedController {

    private final WPI_TalonSRX mSpeedController;
    private final int mSlot = Constants.DEFAULT_PID_SLOT.getValue();
    private double mAllowableError;
    private double mVelocitySetPoint;

    public WPITalonSRKSpeedController(WPI_TalonSRX speedController){
        mSpeedController = speedController;    
    }

    @Override
    public void set(double speed) {        
        mSpeedController.set(speed);
    }

    @Override
    public double get() {      
        return mSpeedController.get();
    }

    @Override
    public void setInverted(boolean isInverted) {        
       mSpeedController.setInverted(isInverted);
    }

    @Override
    public boolean getInverted() {     
        return mSpeedController.getInverted();
    }

    @Override
    public void disable() { 
        mSpeedController.disable();
    }

    @Override
    public void stopMotor() {        
        mSpeedController.stopMotor();
    }

    @Override
    public void pidWrite(double output) {  
        mSpeedController.pidWrite(output);
    }

    @Override
    public void setmotionMagicSetpoint(double setPoint) {
        mSpeedController.set(ControlMode.MotionMagic, setPoint);
    }

    @Override
    public void setVelocitySetpoint(double setPoint) {
        mVelocitySetPoint = setPoint;
        mSpeedController.set(ControlMode.Velocity, setPoint);

    }

    @Override
    public void setP(double p) {
        mSpeedController.config_kP(mSlot, p);

    }

    @Override
    public void setI(double i) {
        mSpeedController.config_kI(mSlot, i);

    }

    @Override
    public void setD(double d) {
        mSpeedController.config_kD(mSlot, d);

    }

    @Override
    public void setIz(double Iz) {
        mSpeedController.config_IntegralZone(mSlot, (int)Iz);

    }

    @Override
    public void setFF(double ff) {
        mSpeedController.config_kF(mSlot, ff);

    }

    @Override
    public void setMaxPIDOutput(double maxOutput) {
        mSpeedController.configClosedLoopPeakOutput(mSlot, maxOutput);

    }

    @Override
    public void setMinPIDOutput(double minOutput) {
        mSpeedController.configClosedLoopPeakOutput(mSlot, minOutput);

    }

    @Override
    public void setMaxVelocity(double maxVelocity) {
        mSpeedController.configMotionCruiseVelocity((int)maxVelocity);

    }

    @Override
    public void setMinVelocity(double minVelocity) {


    }

    @Override
    public void setMaxAcceleration(double maxAcceleration) {
        mSpeedController.configMotionAcceleration((int)maxAcceleration);

    }

    @Override
    public void setAllowedClosedLoopError(double allowedClosedLoopError) {
        mAllowableError = allowedClosedLoopError;
        mSpeedController.configAllowableClosedloopError(mSlot, (int)mAllowableError);

    }

    @Override
    public boolean getAtSetpoint() {
        return mSpeedController.getClosedLoopError() <= mAllowableError;
    }

    @Override
    public boolean getAtVelocitySetpoint(double percentError) {
        return Math.abs((Math.abs(mSpeedController.getSelectedSensorVelocity() - mVelocitySetPoint)) / mVelocitySetPoint) <= percentError;
    }

    

}