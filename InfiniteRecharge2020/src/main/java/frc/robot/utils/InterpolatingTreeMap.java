package frc.robot.utils;

import java.util.Map;
import java.util.TreeMap;

public class InterpolatingTreeMap {

	private TreeMap<Double, Double> mMap;
	private double mOffset;

	public InterpolatingTreeMap(TreeMap<Double, Double> map, double offset) {
		mMap = map;
		mOffset = offset;
	}

	public InterpolatingTreeMap putValue(double key, double value) {
		mMap.put(key, value);
		return this;
	}

	public double getInterpolated (double givenKey) {
		double lowKey = -1.0;
		double lowVal = -1.0;
		for (double mapKey : mMap.keySet()) {
			// System.out.println("key is " + mapKey);
			if (givenKey < mapKey) {
				double highVal = mMap.get(mapKey);
				// System.out.println("high value " + highVal);
				if (lowKey > 0.0) {
					// System.out.println("low key is greater then zero and low key is " + lowKey);
					double m = (givenKey - lowKey) / (mapKey - lowKey);
					return lowVal + m * (highVal - lowVal);
				} else
					// System.out.println("returning high value" + highVal);
				return highVal;

			}
			lowKey = mapKey;
			lowVal = mMap.get(mapKey);
		}

		return mMap.get(mMap.lastKey()) + mOffset;
	}

}
