package frc.robot.subsystems.drive;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.XEncoder;

/**
 * A basic tank DriveBase. It extends our basic DriveBase, adding in encoders
 */
public class TankDriveBase extends DriveBase {

    private final XEncoder mLeftEncoder;
    private final XEncoder mRightEncoder;

    /**
     * Initialize a new drive base with left and right motors as well as encoders
     *
     * @param leftMotor  The left motor(s). Can be an individual speed controller or
     *                   a {@link SpeedControllerGroup}
     * @param rightMotor The right motor(s). Can be an individual speed controller or
     *                   a {@link SpeedControllerGroup}
     * @param leftEncoder The encoder on the left side of the drivebase
     * @param rightEncoder the encoder on the right side of the drivebase
     */
    public TankDriveBase(XSpeedController leftMotor, XSpeedController rightMotor, XEncoder leftEncoder,
        XEncoder rightEncoder, DoubleSolenoid gearShifter) {
        super(leftMotor, rightMotor, gearShifter);
        this.mLeftEncoder = leftEncoder;
        this.mRightEncoder = rightEncoder;
    }

    @Override
    public void initSendable(SendableBuilder builder) {
        super.initSendable(builder);
        this.mLeftEncoder.getSendable().initSendable(builder);
        this.mRightEncoder.getSendable().initSendable(builder);
    }

    public void periodic() {
        super.periodic();
    }

    /**
     * Zeroes both drive encoders
     */
    public void zeroEncoders() {
        this.mLeftEncoder.zero();
        this.mRightEncoder.zero();
    }

    public double getLeftEncoderDistance() {
        return mLeftEncoder.getLinearDistance();
    }

    public double getLeftEncoderVelocity() {
        return mLeftEncoder.getLinearVelocity();
    }

    public double getRightEncoderDistance() {
        return mRightEncoder.getLinearDistance();
    }

    public double getRightEncoderVelocity() {
        return mRightEncoder.getLinearVelocity();
    }

    /**
     * Gets the average of the encoder positions of the left and right encoders
     * @return the average encoder position
     */
    public double getAvgEncoderPosition() {
        double sum = this.getLeftEncoderDistance() + this.getRightEncoderDistance();
        return sum / 2.0;
    }
}
