package frc.robot.subsystems.drive;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.XPneumaticSubsystem;
import frc.robot.utils.controllers.XSpeedController;

/**
 * This class provides a basic implementation of a tank drive. It uses two speed
 * controllers, one for wheels on the left side of a drive base, and one for
 * wheels on the right side. These speed controllers can be master speed
 * controllers, utilizing functionality that already exists in classes such as
 * {@link com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX}, or they could be
 * {@link edu.wpi.first.wpilibj.SpeedControllerGroup}s.
 */
public abstract class DriveBase extends XPneumaticSubsystem {
    // Member variables
    protected final XSpeedController mLeftMotor;
    protected final XSpeedController mRightMotor;

    private double mRightSpeed;
    private double mLeftSpeed;

    /**
     * Initialize a new drive base with left and right motors as well as encoders
     *
     * @param leftMotor  The left motor(s). Can be an individual speedcontroller or
     *                   a {@link edu.wpi.first.wpilibj.SpeedControllerGroup}
     * @param rightMotor The right motor(s). Can be an individual speedcontroller or
     *                   a {@link edu.wpi.first.wpilibj.SpeedControllerGroup}
     */
    public DriveBase(XSpeedController leftMotor, XSpeedController rightMotor, DoubleSolenoid gearShifter) {
        super("Gear Shifter", gearShifter);
        this.mLeftMotor = leftMotor;
        this.mRightMotor = rightMotor;
    }

    /**
     * Set the speed of the motors on the right side of the drive base
     * 
     * @param m_rightJoystick the new motor speed (-1 to 1)
     */
    public void setRightSpeed(double m_rightJoystick) {
        this.mRightSpeed = -m_rightJoystick;
        SmartDashboard.putNumber("Right Speed", mRightSpeed);
        if (Math.abs(this.mRightSpeed) > .08) {
            this.mRightMotor.set(this.mRightSpeed);
        } else {
            mRightMotor.stopMotor();
        }
    }

    /**
     * Set the speed of the motors on the left side of the drive base
     * 
     * @param m_leftJoystick the new motor speed (-1 to 1)
     */
    public void setLeftSpeed(double m_leftJoystick) {
        this.mLeftSpeed = m_leftJoystick;
        SmartDashboard.putNumber("Left speed", mLeftSpeed);
        if (Math.abs(mLeftSpeed) > .08) {
            this.mLeftMotor.set(this.mLeftSpeed);
        } else {
            this.mLeftMotor.stopMotor();
        }
    }

    public void periodic() {
        // Normally would check if the states are different, but here we need to always
        // send a setpoint anyways

    }
}
