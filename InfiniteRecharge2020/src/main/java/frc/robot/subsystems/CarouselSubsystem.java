
package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.controllers.XSpeedController;

public class CarouselSubsystem extends SubsystemBase {
    private final XSpeedController mCarouselMotor;

    public enum Mode {
        Intake,
        Shoot,
        UnJam,
        Off
    };

    public CarouselSubsystem(XSpeedController carouselMotor) {
        super();
        mCarouselMotor = carouselMotor;
    }

    public void intake() {
        mCarouselMotor.set(Constants.CAROUSEL_INTAKE_SPEED);
    }

    public void shoot() {
        mCarouselMotor.set(Constants.CAROUSEL_SHOOT_SPEED);
    }

    public void unJam() {
        mCarouselMotor.set(Constants.CAROUSEL_ANTI_JAM);
    }

    public void stop() {
        mCarouselMotor.set(Constants.MOTOR_STOP_SPEED.getValue());
    }

    public void setSpeed(double speed) {
        mCarouselMotor.set(speed);
    }

    @Override
    public void periodic() {

    }
}
