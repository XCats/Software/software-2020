package frc.robot.commands.climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbingSubsystem;

public class RunClimberCommand extends CommandBase {

    public ClimbingSubsystem mClimbingSubsystem;
    private boolean isFinished;

    public RunClimberCommand(ClimbingSubsystem climbingSubsystem) {
        this.mClimbingSubsystem = climbingSubsystem;
        super.addRequirements(climbingSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        // Climb
        System.out.println("executing winch");
        if(!mClimbingSubsystem.getState()){
            mClimbingSubsystem.extend();
        }

        mClimbingSubsystem.ClimbingMotorOn();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        mClimbingSubsystem.ClimbingMotorOff();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isFinished;
    }
}