/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.carousel;

import frc.robot.subsystems.CarouselSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an example subsystem.
 */
public class CarouselPercentOutputCommand extends CommandBase {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
  private final CarouselSubsystem mCarouselSubsystem;
  private double mSpeed = 0.0;

  /**
   * Creates a new Carousel.
   *
   * @param subsystem The subsystem used by this command.
   */
  public CarouselPercentOutputCommand(CarouselSubsystem carouselSubsystem, double speed) {
    mCarouselSubsystem = carouselSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(mCarouselSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    mCarouselSubsystem.setSpeed(mSpeed);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    mCarouselSubsystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
