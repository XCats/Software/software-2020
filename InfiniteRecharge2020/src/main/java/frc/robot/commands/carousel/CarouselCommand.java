/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.carousel;

import frc.robot.subsystems.CarouselSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an example subsystem.
 */
public class CarouselCommand extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final CarouselSubsystem mCarouselSubsystem;
  private final CarouselSubsystem.Mode mCarouselMode;
  private boolean isFinished = false;
  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public CarouselCommand(CarouselSubsystem subsystem, CarouselSubsystem.Mode carouselMode) {
    mCarouselSubsystem = subsystem;
    mCarouselMode = carouselMode;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("CarouselCommand -- initialized");
  } 

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      //when button is pressed, run motor that rotates the balls around the carousel at a specific speed
    if (mCarouselMode == CarouselSubsystem.Mode.Intake) {
      mCarouselSubsystem.intake();
    } else if (mCarouselMode == CarouselSubsystem.Mode.Shoot) {
      mCarouselSubsystem.shoot();
    } else if (mCarouselMode == CarouselSubsystem.Mode.UnJam) {
      mCarouselSubsystem.unJam();
    }
    else {
      mCarouselSubsystem.stop();
    }
  }


  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("CarouselCommand -- end");
    mCarouselSubsystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return isFinished;
  }
}