/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import frc.robot.commands.carousel.CarouselCommand;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;

/**
 * An example command that uses an example subsystem.
 */
public class UnJamIntakeCommand extends ParallelRaceGroup {
    @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

    /**
     * Creates a new ExampleCommand.
     *
     * @param subsystem The subsystem used by this command.
     */
    public UnJamIntakeCommand(
      CarouselSubsystem carouselSubsystem, AcquisitionRollersSubsystem acquisitionRollersSubsystem) {
    addCommands(new CarouselCommand(carouselSubsystem, CarouselSubsystem.Mode.UnJam),
        new RunOuttakeRollersCommand(acquisitionRollersSubsystem));
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    super.initialize();
    System.out.println("UnJam Command-- initialized");
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    System.out.println("UnJam Command-- end");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}