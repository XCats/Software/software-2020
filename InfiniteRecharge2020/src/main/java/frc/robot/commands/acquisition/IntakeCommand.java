/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import frc.robot.commands.acquisition.RunIntakeRollersCommand;
import frc.robot.commands.carousel.CarouselCommand;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.subsystems.CarouselSubsystem.Mode;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;

/**
 * An example command that uses an example subsystem.
 */
public class IntakeCommand extends ParallelCommandGroup {
    @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

    public IntakeCommand(CarouselSubsystem carouselSubsystem, AcquisitionRollersSubsystem acquisitionRollersSubsystem) {
        addCommands(new CarouselCommand(carouselSubsystem, Mode.Intake), 
          new RunIntakeRollersCommand(acquisitionRollersSubsystem));
    }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("IntakeCommand -- being initialized");
    super.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    super.execute();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    System.out.println("IntakeCommand -- end");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
