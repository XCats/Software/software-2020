package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.ShooterSubsystem;

public class ShootBasic extends CommandBase {

    private final ShooterSubsystem mShooterSubsystem;
    private boolean mIsFinished;

    public ShootBasic(ShooterSubsystem shooterSubSystem) {
        mShooterSubsystem = shooterSubSystem;
        super.addRequirements(shooterSubSystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("Shooter Command -- initialized");
    }

    @Override
    public void execute() {

        // if(mShooterSubsystem.getNumPowerCells() == 0) {

       mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue());
       mShooterSubsystem.setShooterSpeed(Constants.DEFAULT_SHOOTER_OUTPUT);
        }
        //     mIsFinished = true;
        // }
        //mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue());
        // mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue()); // mpk
        //System.out.println("Feed wheel speed: " + Constants.DEFAULT_FEEDER_MOTOR_SPEED);
    
        // mShooterSubsystem.setShooterSpeed(.3);
    

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        System.out.println("Shooter Command -- end");
        mShooterSubsystem.stopFeeder();
        mShooterSubsystem.stopShooter();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }
}