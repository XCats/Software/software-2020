package frc.robot;

public class PortMappings {

	////////////////////////////////
	// CAN Devices
    ////////////////////////////////
	
	// PDP CAN ID - *Must* be 0 if you want to use it to read volts, current, temp, etc.
	public static final int PDP_CAN_ID = 0;

	// Drive Motor CAN IDs
	public static final int DRIVE_MOTOR_RIGHT_MASTER = 1;
    public static final int DRIVE_MOTOR_RIGHT_FOLLOWER = 2;
	public static final int DRIVE_MOTOR_LEFT_MASTER = 3;
	public static final int DRIVE_MOTOR_LEFT_FOLLOWER = 4;
	
	// Power Cell Acquisition CAN IDs
	public static final int ACQ_FRONT2BACK_BELT_MOTOR = 5;
	public static final int ACQ_SIDE2SIDE_BELT_MOTOR = 6;

	// Carousel Motor CAN ID
	public static final int CAROUSEL_MOTOR = 7;

    // Shooter Motor 1 (may be a 2nd for FinalBot)
    public static final int SHOOTER_MOTOR = 8;
    //public static final int SHOOTER2_MOTOR = 9;

	// Turret Swivel Motor CAN ID
	public static final int TURRET_MOTOR = 10;

	// Control Panel Spinner Motor CAN ID
	public static final int CP_SPINNER_MOTOR = 11;

	// Climber Winch Motor CAN ID
	public static final int CLIMBER_WINCH_MOTOR = 12;

	// Shooter Feed Wheel Motor CAN ID
	public static final int SHOOTER_FEED_WHEEL_MOTOR = 13;

	////////////////////////////////
	// PWM
	////////////////////////////////

	// Blinkin
	public static final int BLINKEN_PWM = 3;
	
	////////////////////////////////
	// DIO
	////////////////////////////////

	// Intake Counter Sensors
	public static final int BALL_IN_ACQUISITION = 0;
	
	// Shooter Sensors
	public static final int SHOOTER_SENSOR = 1; // photo sensor

	// Carousel Sensors
	public static final int BALL_IN_CAROUSEL = 2; // photo sensor
	


	////////////////////////////////
	// Pneumatics
	////////////////////////////////

	// Acquisition belt arm deployment up (starting) or down (active)
	public static final int DEPLOY_ACQ_IN_SOLENOID = 2;  // raise / kREVERSE
	public static final int DEPLOY_ACQ_OUT_SOLENOID = 6; // lower / kFORWARD

	// Climber arm lowered or raised
	public static final int CLIMBER_IN_SOLENOID = 5;  // lower / kREVERSE
	public static final int CLIMBER_OUT_SOLENOID = 1; // raise / kFORWARD

	// Transmission gearbox for Drives. OUT = LOW gear, IN is HIGH gear.
	public static final int DRIVES_GEARBOX_OUT_SOLENOID = 3;
	public static final int DRIVES_GEARBOX_IN_SOLENOID = 7;

	// Control Panel spinner arm deployment up (starting) or down (active)
	public static final int DEPLOY_SPINNER_OUT_SOLENOID = 0;  // up / kFORWARD
	public static final int DEPLOY_SPINNER_IN_SOLENOID = 4; // down / kREVERSE


	////////////////////////////////
	// Joysticks
	////////////////////////////////
	public static final int OPERATOR_JS = 0;
	public static final int LEFT_DRIVER_JS = 1;
	public static final int RIGHT_DRIVER_JS = 2;


    ///////////////////////////////
    // Buttons
    ///////////////////////////////
}